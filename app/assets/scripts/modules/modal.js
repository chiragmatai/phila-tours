import $ from 'jquery';

class Modal{
    constructor(){
        this.openModalButton = $(".open-modal");
        this.modal = $(".modal");
        this.closeModalButton = $(".modal__close");
        this.events();
    }
    
    events(){
        this.openModalButton.click(this.openModal.bind(this));
        
        this.closeModalButton.click(this.closeModal.bind(this));
        
        //Keyevent
        $(document).keyup(this.keyPressedHandler.bind(this));
    }
    
    openModal(){
        this.modal.addClass("modal__is-visible");
        return false;
    }
    
    closeModal(){
        this.modal.removeClass("modal__is-visible");
    }
    
    keyPressedHandler(e){
        if(e.keyCode == 27){
            this.closeModal();
        }
    }
}

export default Modal;